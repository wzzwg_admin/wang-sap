package com.wang.sap.controller;

import com.wang.sap.service.IIndexService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author wanggang
 * @date 2023/3/3
 */
@RestController
public class IndexController {

    @Resource
    private IIndexService indexService;

    @GetMapping("/")
    public void index(){
        indexService.zhrfuKqycOa();
    }
}
