import com.sap.conn.jco.JCoDestination;
import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoParameterList;
import com.sap.conn.jco.JCoTable;
import com.wang.sap.util.SapUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * @author wanggang
 * @date 2023/3/3
 */
@Slf4j
public class SapTest {
    public static void main(String[] args) throws Exception {
        String functionName="ZHRFU_KQYC_OA";    //函数名
        String tableName="ET_DATA"; //表名
        JCoDestination jcoConnection = SapUtils.getJcoConnection(functionName);
        JCoFunction function = SapUtils.getJCoFunction(jcoConnection, functionName);
        //获取接口参数格式
        String toXML = function.toXML();
        log.info("接口参数："+toXML);
        //SAP接口参数分为INPUT和TABLES两种，赋值方式不一样
        //INPUT类型的参数赋值
        JCoParameterList inputParams = function.getImportParameterList();
        inputParams.setValue("IV_BEGDA","20230301");
        inputParams.setValue("IV_ENDDA","20230303");
        //TABLES表参数赋值
        JCoTable jCoFields = function.getTableParameterList().getTable(tableName);
        for (int i = 1  ; i <=2; i++) {
            //给表格的第几行写入数据,写入第一行的下标是1
            jCoFields.setRow(i);
            jCoFields.setValue("MANDT","8000");
            jCoFields.setValue("ZINDEX",i);
        }

        function.execute(jcoConnection);
        log.info("返回结果="+function.toXML());
        JCoParameterList tableParameterList = function.getTableParameterList();
        //获取SAP标准返回结果OUTPUT
        Object ev_type = function.getExportParameterList().getValue("EV_TYPE"); //状态码
        Object ev_message = function.getExportParameterList().getValue("EV_MESSAGE");   //文本信息
        //获取SAP返回的表数据,ET_DATA是表名
        JCoTable et_data = tableParameterList.getTable("ET_DATA");
        log.info("et_data="+et_data.toXML());
        int numRows = et_data.getNumRows(); //返回数据行数
        for (int j = 0; j < numRows; j++) {
            //读取数据,第一行的下标是0
            et_data.setRow(j);
            et_data.getString("MANDT");
        }
    }
}
